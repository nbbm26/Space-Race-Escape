# Space-Race-Escape
Needed safe branch to work from

NOTE: must add UFPS and KriptoFX folders into assets to complete

This program WILL run!

Download the build directory and open the .exe !

Controls are as follows:

Level 1:
WASD: forward,strafe left, strafe right, down
space: jump
shift: run
NEED to find the CAR to get to the next level!!

Level 2, Car Chase:
W: acceleration
S: slow and reverse
A, left arrow: turn left
D, right arrow: turn right
mouse wheel: zoom camera
b: reset to last checkpoint
Even though game says gameOver, won't trigger till next level!! then can press b to reset level!

Level 3, Ship combat:
goal is to collect enough purple fuel orbs to jump home!
W: acceleration
S: slow
A: strafe left
D: strafe right
SPACE: SHOOOOOOT!
mouse to steer multi-directionally
B: reset level
Z: low sensitivity steering
X: mid sensitivity steering
C: high sensitivity steering
